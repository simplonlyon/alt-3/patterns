package co.simplon.alt3.entities;

public class Document {
    private String path;
    private String filename;
    private String author;
    private int filesize;
    private String type;
    /**
     * @param path
     * @param filename
     * @param author
     * @param filesize
     * @param type
     */
    public Document(String path, String filename, String author, int filesize, String type) {
        this.path = path;
        this.filename = filename;
        this.author = author;
        this.filesize = filesize;
        this.type = type;
    }

    public Document() {
        
    }

    /**
     * @return the path
     */
    public String getPath() {
        return path;
    }

    /**
     * @param path the path to set
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * @return the filename
     */
    public String getFilename() {
        return filename;
    }

    /**
     * @param filename the filename to set
     */
    public void setFilename(String filename) {
        this.filename = filename;
    }

    /**
     * @return the author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * @param author the author to set
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * @return the filesize
     */
    public int getFilesize() {
        return filesize;
    }

    /**
     * @param filesize the filesize to set
     */
    public void setFilesize(int filesize) {
        this.filesize = filesize;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    
}
