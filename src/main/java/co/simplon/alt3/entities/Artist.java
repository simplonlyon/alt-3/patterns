package co.simplon.alt3.entities;

public class Artist {
    private String name;
    private String instrument;

    public Artist() {

    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the instrument
     */
    public String getInstrument() {
        return instrument;
    }

    /**
     * @param instrument the instrument to set
     */
    public void setInstrument(String instrument) {
        this.instrument = instrument;
    }

    public String toString() {
        return this.name + " fait de la " + this.instrument;
    }
    
}
