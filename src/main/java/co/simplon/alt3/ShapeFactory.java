package co.simplon.alt3;

import co.simplon.alt3.entities.Circle;
import co.simplon.alt3.entities.Square;
import co.simplon.alt3.entities.Triangle;

public class ShapeFactory {

    public static IShape createShape(String shape) throws Exception {
        switch(shape) {
            case "carré" : 
                return new Square();
            case "triangle" : 
                return new Triangle();
            case "cercle" : 
                return new Circle();
            default : 
                throw new Exception("Forme inconnue");
                
        }
    }
    
}
