package co.simplon.alt3.external;

public class Document {
    private String fullpath;
    private String hash;
    /**
     * @param fullpath
     * @param hash
     */
    public Document(String fullpath, String hash) {
        this.fullpath = fullpath;
        this.hash = hash;
    }
    public Document() {

    }
    /**
     * @return the fullpath
     */
    public String getFullpath() {
        return fullpath;
    }
    /**
     * @param fullpath the fullpath to set
     */
    public void setFullpath(String fullpath) {
        this.fullpath = fullpath;
    }
    /**
     * @return the hash
     */
    public String getHash() {
        return hash;
    }
    /**
     * @param hash the hash to set
     */
    public void setHash(String hash) {
        this.hash = hash;
    }
}
