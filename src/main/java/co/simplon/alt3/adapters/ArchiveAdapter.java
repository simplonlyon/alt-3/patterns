package co.simplon.alt3.adapters;

import co.simplon.alt3.entities.Document;
import co.simplon.alt3.external.Archive;

public class ArchiveAdapter {
    private Archive a;

    public ArchiveAdapter(Archive a) {
        this.a = a;
    }

    public void archive(Document de) {
        co.simplon.alt3.external.Document dx = new co.simplon.alt3.external.Document();
        dx.setFullpath(de.getPath() + de.getFilename());
        dx.setHash("hash");
        a.archive(dx);
    }
}
