package co.simplon.alt3;

import java.util.ArrayList;
import java.util.List;


//Singleton
public class BreedManager {
    
    private static BreedManager instance;
    private List<String> breedList;

    private BreedManager() {
        this.breedList = new ArrayList<>();
    }

    public static BreedManager getInstance() {
        if (instance == null)
            instance = new BreedManager();
        return instance;
    }

    public void add(String breed) {
        this.breedList.add(breed);
        return;
    }

    public String toString() {
        return this.breedList.toString();
    }

}
