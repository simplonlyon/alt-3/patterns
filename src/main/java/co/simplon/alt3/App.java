package co.simplon.alt3;
import co.simplon.alt3.adapters.ArchiveAdapter;
import co.simplon.alt3.entities.Artist;
import co.simplon.alt3.entities.Document;
import co.simplon.alt3.external.Archive;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {

        return;
    }

    public static void testSingleton() {
        System.out.println( "Test Singleton !" );
        BreedManager b1 = BreedManager.getInstance();
        System.out.println("b1 : " + b1);
        BreedManager b2 = BreedManager.getInstance();
        System.out.println("b2 : " + b2);
        b1.add("Épagneul");
        System.out.println("b2 : " + b2);
        System.out.println("Test du builder");
        Artist singer = ArtistBuilder.getSinger("Prince");
        Artist guitarist = ArtistBuilder.getGuitarist("Hendrix");
        Artist generic = ArtistBuilder.getArtist("Ringo Starr", "batterie");
        System.out.println(singer);
        System.out.println(guitarist);
        System.out.println(generic);
    }

    public static void testFactory() {
        System.out.println("Test de la factory");

        try {
            IShape s = ShapeFactory.createShape("carré");
            s.draw();
            s = ShapeFactory.createShape("triangle");
            s.draw();
            s = ShapeFactory.createShape("cercle");
            s.draw();
            s = ShapeFactory.createShape("rectangle");
            s.draw();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void testAdapter() {
        System.out.println("Test de l'adapter");
        Document d = new Document();
        d.setFilename("Facture-0017.pdf");
        d.setPath("/srv/myapp/input/");
        ArchiveAdapter aa = new ArchiveAdapter((new Archive()));
        aa.archive(d);
    }
}
