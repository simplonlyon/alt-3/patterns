package co.simplon.alt3;

import co.simplon.alt3.entities.Artist;

public class ArtistBuilder {
    
    public static Artist getArtist(String name, String instrument) {
        Artist a = new Artist();
        a.setName(name);
        a.setInstrument(instrument);
        return a;
    }

    public static Artist getSinger(String name) {
        return ArtistBuilder.getArtist(name, "voix");
    }

    public static Artist getGuitarist(String name) {
        return ArtistBuilder.getArtist(name, "guitare");
    }

}
